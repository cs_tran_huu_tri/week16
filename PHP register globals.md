# PHP register globals 

Theo gợi ý của đề bài, có file backup, nhập thêm vào URL index.php.bak, down về, mở ra, ta có :

```php
if (( isset ($password) && $password!="" && auth($password,$hidden_password)==1) || (is_array($_SESSION) && $_SESSION["logged"]==1 ) ){
    $aff=display("well done, you can validate with the password : $hidden_password");
} else {
    $aff=display("try again");
}
```

Vậy không cần quan tâm đến pass, ta chỉ cần chỉnh sao cho $_SESSION["logged"] ==1, dùng Burp Suite thôi:

![Screenshot from 2018-09-03 10-46-12](/home/tritranhuu/Desktop/root me/pic/Screenshot from 2018-09-03 10-46-12.png)

---

### Flag:

`NoTQYipcRKkgrqG`