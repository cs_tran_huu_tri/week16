# HTTP-Open redirect

Ctrl + U ta có:

```html
<a href='?url=https://facebook.com&h=a023cfbf5f1c39bdf8407f28b60cd134'>facebook</a>
<a href='?url=https://twitter.com&h=be8b09f7f1f66235a9c91986952483f0'>twitter</a>
<a href='?url=https://slack.com&h=e52dc719664ead63be3d5066c135b6da'>slack</a>
```

Khi bấm vào một trong các hộp có trên trang web, nó sẽ chuyển đến trang web tương ứng được gán cho tham số `url`, còn tham số `h` là mã md5 của `url`

Muốn chuyển hướng đến google, ta sử dụng url sau:

http://challenge01.root-me.org/web-serveur/ch52/?url=https://google.com&h=99999ebcfdb78df077ad2727fd00969f

Bắt gói tin trả về và nhận được flag.

---

### Flag:

`e6f8a530811d5a479812d7b82fc1a5c5`