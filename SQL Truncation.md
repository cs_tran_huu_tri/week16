# SQL Truncation

Vào đây xem source:

`view-source:http://challenge01.root-me.org/web-serveur/ch36/register.php`

Ta có:

```
CREATE TABLE IF NOT EXISTS user(   
	id INT NOT NULL AUTO_INCREMENT,
    login VARCHAR(12),
    password CHAR(32),
    PRIMARY KEY (id));
```

Bây giờ đăng kí với nick là 'admin       123' và pass là '12345678'

vào page của admin, gõ pass là 12345678

---

### Flag:

`J41m3Qu4nD54Tr0nc`