# XML External Entity

Copy đường link sau và cho vào cái box rồi submit:

`https://gitlab.com/tritranhuu/tet/raw/master/attack.rss`

Có đoạn base64, decode ta được:

```
$pass === "c934fed17f1cac3045ddfeca34f332bc"
```

---

### Flag:

`c934fed17f1cac3045ddfeca34f332bc`