# PHP type juggling

Xem source code, ta có 

```php
if($auth['data']['login'] == $USER && !strcmp($auth['data']['password'], $PASSWORD_SHA256)){
        $return['status'] = "Access granted! The validation password is: $FLAG";
    }
```

Ở điều kiện đầu, ta thấy PHP Loose Comparison, vế phải là 1 string thì ta cho vế trái =0 là trả về true

Điều kiện thứ 2 dùng strcmp, nếu `$auth['data']['password']` là NULL thì điều kiện 2 cũng là TRUE

Chuyển về dạng JSON :`{"data":{"login":true,"password":[]}}`

URL encode và gán cho auth trên Burp:

![Screenshot from 2018-09-03 23-20-32](/home/tritranhuu/Desktop/root me/pic/Screenshot from 2018-09-03 23-20-32.png)

---

### Flag:

`DontForgetPHPL00seComp4r1s0n`