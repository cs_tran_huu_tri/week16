# PHP - Loose Comparison

Xem source code có đoạn so sánh ko chặt chẽ như sau:

```php
if (isset($_POST['s']) && isset($_POST['h'])) {
    $s = sanitize_user_input($_POST['s']);
    $h = secured_hash_function($_POST['h']);
    $r = gen_secured_random();
    if($s != false && $h != false) {
        if($s.$r == $h) {
            print "Well done! Here is your flag: ".$flag;
        }
        else {
            print "Fail...";
        }
    }
    else {
        print "<p>Hum ...</p>";
    }
}
```

$s là gía trị nhập trong ô seed, $r là random cái gì đó, $h là md5 của giá trị trong ô hash. Lợi dụng PHP - Loose Comparison, ta sẽ nhập vào ô seed 0e, ô hash 1 giá trị mà khi md5 hash ra chuỗi có dạng 0e... , google là `240610708 `

---

### Flag:

`F34R_Th3_L0o5e_C0mP4r15On`

