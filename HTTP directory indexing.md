# HTTP directory indexing

Ctrl + U ta có:

Nhìn thấy có include("admin/pass.html")

Thử thêm admin vào path trên URL, ta nhìn thấy index

Mò được file có đường dẫn như sau `http://challenge01.root-me.org/web-serveur/ch4/admin/backup/admin.txt`

---

### Flag:

`LINUX`